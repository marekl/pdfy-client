package pdfy

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
)

// Options for PDFy API
type Options struct {
	margins []int
}

// Client for PDFy
type Client struct {
	apiKey  string
	host    string
	options Options
}

// NewClient returns a new client instance
func NewClient(host string, apiKey string) (*Client, error) {
	if host == "" {
		return nil, errors.New("No host provided for pdfy.Client")
	}
	return &Client{
		apiKey: apiKey,
		host:   host,
		options: Options{
			margins: []int{10, 10, 10, 10},
		},
	}, nil
}

// SetMargins sets document margins
func (c *Client) SetMargins(top int, right int, bottom int, left int) {
	c.options.margins = []int{top, right, bottom, left}
}

func (c *Client) getURL() *url.URL {
	return &url.URL{
		Scheme: "https",
		Host:   c.host,
		Path:   "generate",
	}
}

// GetPDF returns a PDF for a HTML document
// Do not forget to close the body
func (c *Client) GetPDF(document io.Reader) (io.ReadCloser, error) {
	// Create request
	req, err := http.NewRequest("POST", c.getURL().String(), document)
	if err != nil {
		return nil, err
	}

	// Add headers
	req.Header.Add("Content-Type", "text/html; charset=utf-8")
	req.Header.Add("X-API-Key", c.apiKey)
	req.Header.Add("X-Margins", fmt.Sprintf("%d %d %d %d", c.options.margins[0], c.options.margins[1], c.options.margins[2], c.options.margins[3]))

	// Do and evaluate errors
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	if res.StatusCode == http.StatusUnauthorized || res.StatusCode == http.StatusForbidden {
		return nil, errors.New("Unauthorized request to PDFy API")
	}
	if res.StatusCode != http.StatusOK {
		return nil, errors.New("Unknown error from PDFy API")
	}

	// Return body
	return res.Body, nil
}
