VERSION=1.6.0

BUILDTAGS=debug

GOCMD?=$(shell which go)
GOBUILD=$(GOCMD) build
GOGENERATE=$(GOCMD) generate
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOLINT?=$(shell which golint)
GOGET=$(GOCMD) get
DOCKER=docker

MODULE=pdfy-client
PACKAGE=gitlab.com/marekl/pdfy-client

ifeq ($(PREFIX),)
	PREFIX := /usr/local
endif

.PHONY: dep clean test coverage coverhtml lint msan race

test: dep ## Run unittests
	$(GOTEST) -short $(PACKAGE)/...

race: dep ## Run data race detector
	$(GOTEST) -race -short $(PACKAGE)/...

msan: dep ## Run memory sanitizer
	$(GOTEST) -msan -short $(PACKAGE)/...

coverage: ## Generate global code coverage report
	./tools/coverage.sh;

coverhtml: ## Generate global code coverage report in HTML
	./tools/coverage.sh html;

lint: dep ## Lint the files
	$(GOLINT) -set_exit_status ./...

clean: ## Remove previous build files
	$(GOCLEAN) ./...
	rm -rf bin/* vendor

dep: ## Get the dependencies
	$(GOGET) -v -d ./...

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'